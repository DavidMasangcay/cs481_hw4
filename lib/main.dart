import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/animation.dart';
import 'package:flutter/services.dart';

bool clicked = false;

void main() {
  runApp(LogoApp());
}

//Wanted to create a class that could be called and uses a button
//It would wait for a user to press it then start the animation
/*
class Button extends StatefulWidget {
  @override
  _Button createState() => _Button();
}

class _Button extends State<Button> {
  @override

  Widget build(BuildContext context) {
      return Container(
        child: FloatingActionButton(
          backgroundColor: Colors.green,
          onPressed: (){clicked = true;},
        ),
      );
  }
}
 */

class LogoApp extends StatefulWidget {
  _LogoAppState createState() => _LogoAppState();
}

class _LogoAppState extends State<LogoApp> with SingleTickerProviderStateMixin {
  Animation<double> animation;
  AnimationController controller;

  @override
  void initState() {
    super.initState();
    controller = AnimationController(duration: const Duration(seconds: 2), vsync: this);
    animation = CurvedAnimation(parent: controller, curve: Curves.easeIn)
      ..addStatusListener((status) {
        if(status == AnimationStatus.completed)
          controller.reverse();

        else if(status == AnimationStatus.dismissed)
          controller.forward();
      })
      ..addStatusListener((state) => print('$state'));

    controller.forward();
  }

  @override
  Widget build(BuildContext context) => AnimatedLogo(animation: animation);

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }
}

class AnimatedLogo extends AnimatedWidget {
  static final  _opacityTween = Tween<double>(begin: 0.1, end: 1);
  static final  _sizeTween = Tween<double>(begin: 0, end: 300);

  AnimatedLogo({Key key, Animation<double> animation})
      : super(key: key, listenable: animation);

  Widget build(BuildContext context) {
    final animation = listenable as Animation<double>;
    return Center(
      child:Opacity(
        opacity: _opacityTween.evaluate(animation),
        child: Container(
          margin: EdgeInsets.symmetric(vertical: 10),
          height: _sizeTween.evaluate(animation),
          width: _sizeTween.evaluate(animation),
          child: Image.asset('images/waffle.jpg', width: 100, height: 100, fit: BoxFit.cover,),
        ),
      ),
    );
  }
}